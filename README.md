# README #

Test application that connects to an API and retrieves via a GET REST call (with header parameters) an array of 10 images in a JSON format.
Those images are received in an Base64 encoded format, so they need to be decoded and then displayed on a list.
The application is required to perform paging, so once the user scrolls at the end of the first batch of images, a new request is done to retrieve other 10 images
Each time that some images are downloaded, they are also stored in a local DB for offline consultation (of course the images are not stored in the DB itself since storing bitmap is rather inefficient, but their raw data is stored)
The list has also a pull to refresh functionality that is supposed to download newly inserted images on the server side


### Set up ###

I set up the application to work with activities holding fragments with a one to one pairing.
This only has one activity holding one fragment, but the pattern can be extended to bigger project where each activity is a mere vessel where a fragment is attached, and by setting the retainInstance option to true we make sure the fragment is not destroyed on events like change of configurations, hence assuring that the callback for the API call is gonna fall back to the proper place.
Google encourages the use of fragments with this option set on true for this kind of operations, although they mention that this should be performed with fragment not dealing with UI.
An alternative could be the use of libraries like Volley (also google) or Otto/EventBus to register an activity/fragment to a bus of events (singleton instantiated with the application) where callbacks will be redirected once done

Another architectural pattern used here is the presence of Managers and Daos, the first to take care of manipulations on the results coming form APIs, and the latter for actually retrieving the data in a user-agnostic way (working with interfaces there could be several DAO implementations from an API, from a DB, from a file in memory, etc)
Arguably, the managers could be skipped as a whole, and the functionalities to manipulate the data and present them in the proper format to the UI could be contained in the Data-Object itself

I decided to ditch the style AppCompat and having an action bar (although kind of not in line with Google guidelines) because in this specific case it did not make sense to use that much screen space for something that didn't really add functionalities for the user


### Libraries ###

* Android support -> to use pull to refresh functionality (and possible appCompat design)
* Retrofit -> Rest API connection layers (first time using it)
* Weak Handler from Badoo -> for the pull to refresh functionality, although as explained in MainFragment other solutions could be adopted
* DBFlow -> for the DB and offline consultation