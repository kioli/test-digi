package kioli.digi.db;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

@Table(databaseName = DigiDatabase.NAME)
public class MyImageResultDB extends BaseModel {

    @Column
    @PrimaryKey
    private String imageId;
    @Column
    private String imageText;
    @Column
    private float imageConfidence;
    @Column
    private String imageData;

    public MyImageResultDB() {
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImageText() {
        return imageText;
    }

    public void setImageText(String imageText) {
        this.imageText = imageText;
    }

    public float getImageConfidence() {
        return imageConfidence;
    }

    public void setImageConfidence(float imageConfidence) {
        this.imageConfidence = imageConfidence;
    }

    public String getImageData() {
        return imageData;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData;
    }
}
