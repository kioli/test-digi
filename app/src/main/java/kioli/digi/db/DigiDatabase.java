package kioli.digi.db;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = DigiDatabase.NAME, version = DigiDatabase.VERSION)
public class DigiDatabase {

    public static final String NAME = "DigIdentity";

    public static final int VERSION = 1;
}