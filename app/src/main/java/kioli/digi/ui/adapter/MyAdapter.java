package kioli.digi.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import kioli.digi.R;
import kioli.digi.ui.model.MyImageListModel;

public class MyAdapter extends BaseAdapter {

    private final ArrayList<MyImageListModel> data;

    public MyAdapter(final ArrayList<MyImageListModel> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public MyImageListModel getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final myHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            holder = new myHolder();
            holder.image = (ImageView) convertView.findViewById(R.id.list_image);
            holder.description = (TextView) convertView.findViewById(R.id.item_description);
            holder.confidence = (TextView) convertView.findViewById(R.id.item_confidence);
            holder.id = (TextView) convertView.findViewById(R.id.item_id);
            convertView.setTag(holder);
        } else {
            holder = (myHolder) convertView.getTag();
        }

        MyImageListModel myImage = data.get(position);
        holder.image.setImageBitmap(myImage.getImage());
        holder.description.setText(myImage.getDescription());
        holder.confidence.setText(String.valueOf(myImage.getConfidence()));
        holder.id.setText(myImage.getImageId());

        return convertView;
    }

    private static class myHolder {
        private ImageView image;
        private TextView description;
        private TextView confidence;
        private TextView id;
    }
}