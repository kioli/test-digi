package kioli.digi.ui.model;

import android.graphics.Bitmap;

public class MyImageListModel {

    private Bitmap image;
    private String description;
    private float confidence;
    private String id;

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getConfidence() {
        return confidence;
    }

    public void setConfidence(float confidence) {
        this.confidence = confidence;
    }

    public String getImageId() {
        return id;
    }

    public void setImageId(String id) {
        this.id = id;
    }

}
