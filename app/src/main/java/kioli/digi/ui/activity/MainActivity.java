package kioli.digi.ui.activity;

import android.app.Activity;
import android.os.Bundle;

import kioli.digi.R;
import kioli.digi.ui.fragment.MainFragment;


public class MainActivity extends Activity {

    public final static String FRAGMENT_TAG = "fragment tag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new MainFragment(), FRAGMENT_TAG)
                    .commit();
        }
    }
}
