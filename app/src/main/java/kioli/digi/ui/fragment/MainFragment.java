package kioli.digi.ui.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.badoo.mobile.util.WeakHandler;

import java.util.ArrayList;
import java.util.List;

import kioli.digi.R;
import kioli.digi.data.ResultHandler;
import kioli.digi.data.response.MyImageResult;
import kioli.digi.helper.MyImageItemHelper;
import kioli.digi.ui.adapter.MyAdapter;
import kioli.digi.ui.model.MyImageListModel;
import kioli.digi.utils.ConnectionStatusUtil;
import kioli.digi.wiring.ClassWiring;

public class MainFragment extends Fragment implements OnRefreshListener, OnScrollListener, View.OnClickListener, ResultHandler {

    private LinearLayout layoutLoading;
    private LinearLayout layoutError;
    private SwipeRefreshLayout layoutList;

    private ListView list;

    private ArrayList<MyImageListModel> data = new ArrayList<>();
    private MyAdapter adapter;

    private int preLast;

    // https://techblog.badoo.com/blog/2014/08/28/android-handler-memory-leaks
    //
    // As an alternative we could have used a static inner class for the handler
    // (and the runnable) holding a WeakReference to the fragment
    private final WeakHandler handler = new WeakHandler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        layoutList = (SwipeRefreshLayout) view.findViewById(R.id.list_layout);
        layoutList.setOnRefreshListener(this);
        list = (ListView) view.findViewById(R.id.list);

        view.findViewById(R.id.button_retry).setOnClickListener(this);
        view.findViewById(R.id.button_settings).setOnClickListener(this);

        layoutError = (LinearLayout) view.findViewById(R.id.error_layout);
        layoutLoading = (LinearLayout) view.findViewById(R.id.loading_layout);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new MyAdapter(data);
        list.setAdapter(adapter);
        list.setOnScrollListener(this);

        // Load images either from DB (exception if DB not found) or from server
        List<MyImageResult> listImages = new ArrayList<>();
        try {
            listImages = MyImageItemHelper.readMyImageResultFromDB();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (listImages.size() == 0) {
            getImagesFromServer(null, null);
        } else {
            List<MyImageListModel> convertedList = ClassWiring.getConnectionManager(this).convertMyImageList(listImages);
            addNonDuplicateItemsToTheList(convertedList);
        }
    }

    private void stopRefreshingAnimation() {
        if (layoutList.isRefreshing()) {
            layoutList.setRefreshing(false);
        }
    }

    private void getImagesFromServer(final String maxId, final String sinceId) {
        if (ConnectionStatusUtil.hasInternetConnection(getActivity())) {
            if (maxId == null && sinceId == null) {
                showLoadingPage();
            }
            ClassWiring.getConnectionManager(this).getListMyImages(maxId, sinceId);
        } else {
            if (data.size() > 0) {
                stopRefreshingAnimation();
                Toast.makeText(getActivity(), getString(R.string.error_connection_message), Toast.LENGTH_SHORT).show();
            } else {
                showErrorPage();
            }
        }
    }

    private void showLoadingPage() {
        layoutError.setVisibility(View.GONE);
        layoutList.setVisibility(View.GONE);
        layoutLoading.setVisibility(View.VISIBLE);
    }

    private void showListPage() {
        layoutError.setVisibility(View.GONE);
        layoutList.setVisibility(View.VISIBLE);
        layoutLoading.setVisibility(View.GONE);
    }

    private void showErrorPage() {
        layoutError.setVisibility(View.VISIBLE);
        layoutList.setVisibility(View.GONE);
        layoutLoading.setVisibility(View.GONE);
    }

    private void addNonDuplicateItemsToTheList(List<MyImageListModel> convertedList) {
        for (MyImageListModel listItem : convertedList) {
            if (!isItemAlreadyPresent(listItem)) {
                data.add(listItem);
            }
        }
        adapter.notifyDataSetChanged();
        showListPage();
    }

    private boolean isItemAlreadyPresent(MyImageListModel resultImage) {
        for (MyImageListModel image : data) {
            if (image.getImageId().equalsIgnoreCase(resultImage.getImageId()))
                return true;
        }
        return false;
    }

    private void loadMore() {
        String lastItemId = adapter.getItem(adapter.getCount() - 1).getImageId();
        getImagesFromServer(lastItemId, null);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_retry:
                getImagesFromServer(null, null);
                break;
            case R.id.button_settings:
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                break;
            default:
                break;
        }
    }

    @Override
    public void onRefresh() {
        layoutList.setRefreshing(true);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                String firstItemId = adapter.getItem(0).getImageId();
                getImagesFromServer(null, firstItemId);
            }
        }, 1000);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // Not used
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        final int lastVisibleItem = firstVisibleItem + visibleItemCount;
        if (lastVisibleItem == totalItemCount && preLast != lastVisibleItem && totalItemCount != 0) {
            preLast = lastVisibleItem;
            loadMore();
        }
    }

    @Override
    public void onRightResult(List<MyImageListModel> data) {
        stopRefreshingAnimation();
        addNonDuplicateItemsToTheList(data);
    }

    @Override
    public void onErrorResult(String error) {
        stopRefreshingAnimation();
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }
}
