package kioli.digi.data.response;

import com.google.gson.annotations.SerializedName;

public class MyImageResult {

    @SerializedName("_id")
    private String imageId;
    @SerializedName("text")
    private String imageText;
    @SerializedName("confidence")
    private float imageConfidence;
    @SerializedName("img")
    private String imageData;

    public MyImageResult() {
    }

    public String getImageText() {
        return imageText;
    }

    public void setImageText(String imageText) {
        this.imageText = imageText;
    }

    public float getImageConfidence() {
        return imageConfidence;
    }

    public void setImageConfidence(float imageConfidence) {
        this.imageConfidence = imageConfidence;
    }

    public String getImageData() {
        return imageData;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }
}
