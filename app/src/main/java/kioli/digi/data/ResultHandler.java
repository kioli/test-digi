package kioli.digi.data;

import java.util.List;

import kioli.digi.ui.model.MyImageListModel;

public interface ResultHandler {
    void onRightResult(List<MyImageListModel> data);

    void onErrorResult(String error);
}
