package kioli.digi.data.manager.impl;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.util.ArrayList;
import java.util.List;

import kioli.digi.data.ResultHandler;
import kioli.digi.data.dao.MyImageDao;
import kioli.digi.data.manager.ConnectionManager;
import kioli.digi.data.response.MyImageResult;
import kioli.digi.helper.MyImageItemHelper;
import kioli.digi.ui.model.MyImageListModel;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ConnectionManagerImpl implements ConnectionManager {

    private final MyImageDao myImageDao;
    private final Callback<List<MyImageResult>> callback;

    public ConnectionManagerImpl(final MyImageDao myImageDao, final ResultHandler handler) {
        super();
        this.myImageDao = myImageDao;

        callback = new Callback<List<MyImageResult>>() {
            @Override
            public void success(List<MyImageResult> result, Response response2) {
                List<MyImageListModel> convertedList = new ArrayList<>();
                if (result != null) {
                    convertedList = convertMyImageList(result);
                }
                handler.onRightResult(convertedList);
            }

            @Override
            public void failure(RetrofitError error) {
                handler.onErrorResult(error.getMessage());
            }
        };
    }

    @Override
    public void getListMyImages(final String maxId, final String sinceId) {
        myImageDao.retrieveData(maxId, sinceId, callback);
    }

    @Override
    public List<MyImageListModel> convertMyImageList(List<MyImageResult> listImages) {
        List<MyImageListModel> convertedList = new ArrayList<>();
        for (MyImageResult listItem : listImages) {
            MyImageItemHelper.storeMyImageResultIntoDB(listItem);

            byte[] decoded = Base64.decode(listItem.getImageData(), Base64.DEFAULT);
            Bitmap bmp = BitmapFactory.decodeByteArray(decoded, 0, decoded.length);

            MyImageListModel image = new MyImageListModel();
            image.setImage(bmp);
            image.setConfidence(listItem.getImageConfidence());
            image.setDescription(listItem.getImageText());
            image.setImageId(listItem.getImageId());

            convertedList.add(image);
        }
        return convertedList;
    }
}
