package kioli.digi.data.manager;

import java.util.List;

import kioli.digi.data.response.MyImageResult;
import kioli.digi.ui.model.MyImageListModel;

/**
 * Csv file manager
 */
public interface ConnectionManager {

    /**
     * Retrieve the list of MyImage items
     */
    void getListMyImages(String maxId, String sinceId);

    /**
     * Converts the list of MyImage objects into a list of MyImageListModel objects
     */
    List<MyImageListModel> convertMyImageList(List<MyImageResult> listImages);

}
