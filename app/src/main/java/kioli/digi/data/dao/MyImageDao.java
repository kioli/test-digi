package kioli.digi.data.dao;

import java.util.List;

import kioli.digi.data.response.MyImageResult;
import retrofit.Callback;

/**
 * Dao to retrieve a list of MyImage
 */
public interface MyImageDao {

    /**
     * Retrieve a list of MyImage items
     */
    void retrieveData(String maxId, String sinceId, Callback<List<MyImageResult>> callback);

}
