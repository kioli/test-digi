package kioli.digi.data.dao.impl;

import java.util.List;

import kioli.digi.data.connection.DigiApi;
import kioli.digi.data.connection.ServiceGenerator;
import kioli.digi.data.dao.MyImageDao;
import kioli.digi.data.response.MyImageResult;
import retrofit.Callback;

public class MyImageDaoImpl implements MyImageDao {

    public MyImageDaoImpl() {
    }

    @Override
    public void retrieveData(final String maxId, final String sinceId, Callback<List<MyImageResult>> callback) {
        DigiApi api = ServiceGenerator.createService(DigiApi.class);
        api.getImages(maxId, sinceId, callback);
    }
}
