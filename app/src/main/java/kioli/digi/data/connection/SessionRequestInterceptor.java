package kioli.digi.data.connection;

import retrofit.RequestInterceptor;

class SessionRequestInterceptor implements RequestInterceptor {
    @Override
    public void intercept(RequestFacade request) {
        request.addHeader("Client-secret", "248d3d3a7d0a6b4e1e6e81f28a001b17");
        request.addHeader("Cache-Control", "public");
    }
}