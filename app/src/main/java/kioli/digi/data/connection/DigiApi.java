package kioli.digi.data.connection;

import java.util.List;

import kioli.digi.data.response.MyImageResult;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface DigiApi {

    /**
     * API call for retrieving images
     *
     * @param maxId The ID of the most recent item (if specified it returns results older than or equal to the specified ID)
     * @param sinceId The ID of the oldest item (if specified it returns results more recent than the specified ID)
     * @param response callback returning a list of 10 MyImageResult
     */
    @GET("/items")
    void getImages(@Query("max_id") String maxId, @Query("since_id") String sinceId, Callback<List<MyImageResult>> response);
}