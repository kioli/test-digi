package kioli.digi.data.connection;

import retrofit.RestAdapter;

public class ServiceGenerator {

    private final static String BASE_URL = "https://marlove.net/mock/v1";

    private ServiceGenerator() {}

    public static <S> S createService(Class<S> serviceClass) {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setRequestInterceptor(new SessionRequestInterceptor());

        RestAdapter adapter = builder.build();

        return adapter.create(serviceClass);
    }
}