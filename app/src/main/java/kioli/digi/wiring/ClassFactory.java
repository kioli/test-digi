package kioli.digi.wiring;

import kioli.digi.data.ResultHandler;
import kioli.digi.data.dao.MyImageDao;
import kioli.digi.data.manager.ConnectionManager;

/**
 * Class factory
 */
interface ClassFactory {

	/**
	 * Get the connection manager
	 *
	 * @param  myImageDao dao to retrieve data
	 * @param  handler to bring back the result to the entity asking for it
	 * @return CsvManager
	 */
	ConnectionManager getConnectionManager(MyImageDao myImageDao, ResultHandler handler);

	/**
	 * Get the csv file manager
	 *
	 * @return CsvDao
	 */
	MyImageDao getMyImageDao();
}
