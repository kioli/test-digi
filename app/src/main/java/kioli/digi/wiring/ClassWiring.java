package kioli.digi.wiring;

import kioli.digi.data.ResultHandler;
import kioli.digi.data.dao.MyImageDao;
import kioli.digi.data.manager.ConnectionManager;

public class ClassWiring {

    private static ClassFactory classFactory;
    private static ConnectionManager connectionManager;
    private static MyImageDao myImageDao;

    private ClassWiring() {
    }

    /**
     * Set the class factory. Easy to switch to debug or mock mode
     *
     * @param classFactory factory for managers and daos
     */
    public static void setClassFactory(final ClassFactory classFactory) {
        ClassWiring.classFactory = classFactory;
        connectionManager = null;
        myImageDao = null;
    }

    public static ConnectionManager getConnectionManager(ResultHandler handler) {
        if (connectionManager == null) {
            connectionManager = classFactory.getConnectionManager(getMyImageDao(), handler);
        }
        return connectionManager;
    }

    private static MyImageDao getMyImageDao() {
        if (myImageDao == null) {
            myImageDao = classFactory.getMyImageDao();
        }
        return myImageDao;
    }
}
