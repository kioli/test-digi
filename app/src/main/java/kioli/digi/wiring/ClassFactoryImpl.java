package kioli.digi.wiring;

import kioli.digi.data.ResultHandler;
import kioli.digi.data.dao.MyImageDao;
import kioli.digi.data.dao.impl.MyImageDaoImpl;
import kioli.digi.data.manager.ConnectionManager;
import kioli.digi.data.manager.impl.ConnectionManagerImpl;

public class ClassFactoryImpl implements ClassFactory {

    public ClassFactoryImpl() {
    }

    @Override
    public ConnectionManager getConnectionManager(MyImageDao myImageDao, ResultHandler handler) {
        return new ConnectionManagerImpl(myImageDao, handler);
    }

    @Override
    public MyImageDao getMyImageDao() {
        return new MyImageDaoImpl();
    }

}
