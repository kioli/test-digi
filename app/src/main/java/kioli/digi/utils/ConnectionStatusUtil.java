package kioli.digi.utils;

import android.content.Context;
import android.net.ConnectivityManager;

public final class ConnectionStatusUtil {

	private ConnectionStatusUtil() {
	}

	/**
	 * Check if the device is connected to the internet
	 * 
	 * @param context context
	 * @return true if connected, false otherwise
	 */
	public static boolean hasInternetConnection(final Context context) {
		final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm.getActiveNetworkInfo() != null
				&& cm.getActiveNetworkInfo().isAvailable()
				&& cm.getActiveNetworkInfo().isConnected()) {
			return true;
		}
		return false;
	}


}
