package kioli.digi;

import android.app.Application;

import com.raizlabs.android.dbflow.config.FlowManager;

import kioli.digi.wiring.ClassFactoryImpl;
import kioli.digi.wiring.ClassWiring;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ClassWiring.setClassFactory(new ClassFactoryImpl());
        FlowManager.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        FlowManager.destroy();
    }
}
