package kioli.digi.helper;

import com.raizlabs.android.dbflow.sql.language.Select;

import java.util.ArrayList;
import java.util.List;

import kioli.digi.data.response.MyImageResult;
import kioli.digi.db.MyImageResultDB;

public class MyImageItemHelper {

    private MyImageItemHelper(){}

    public static void storeMyImageResultIntoDB(MyImageResult item) {
        MyImageResultDB itemDb = new MyImageResultDB();
        itemDb.setImageConfidence(item.getImageConfidence());
        itemDb.setImageData(item.getImageData());
        itemDb.setImageId(item.getImageId());
        itemDb.setImageText(item.getImageText());
        itemDb.save();
    }

    public static List<MyImageResult> readMyImageResultFromDB() {
        List<MyImageResultDB> listImages = new Select().from(MyImageResultDB.class).queryList();
        List<MyImageResult> resultList = new ArrayList<>();
        for (MyImageResultDB dbItem : listImages) {
            MyImageResult resultItem = new MyImageResult();
            resultItem.setImageText(dbItem.getImageText());
            resultItem.setImageConfidence(dbItem.getImageConfidence());
            resultItem.setImageId(dbItem.getImageId());
            resultItem.setImageData(dbItem.getImageData());
            resultList.add(resultItem);
        }
        return resultList;
    }
}
